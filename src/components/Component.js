export default class Component {
	tagName;
	children;
	attribute;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		if (attribute != null) {
			this.attribute = attribute;
		}
		if (children != null) {
			this.children = children;
		}
	}
	render() {
		if (this.attribute == null) {
			let temp = this.renderChildren();
			return temp;
		} else {
			if (this.children == null) {
				return (
					'<' +
					this.tagName +
					' ' +
					this.attribute.name +
					'="' +
					this.attribute.value +
					'" />'
				);
			} else {
				let tmp = this.renderAll();
				return tmp;
			}
		}
	}
	renderChildren() {
		if (this.children == null) {
			return '<' + this.tagName + '></' + this.tagName + '>';
		} else {
			if (this.children instanceof Array) {
				let tmp = '';
				for (let i = 0; i < this.children.length; i++) {
					tmp += this.children[i];
				}
				return '<' + this.tagName + '>' + tmp + '</' + this.tagName + '>';
			} else {
				return (
					'<' + this.tagName + '>' + this.children + '</' + this.tagName + '>'
				);
			}
		}
	}
	renderAll() {
		if (this.children instanceof Component) {
			return (
				'<' +
				this.tagName +
				' ' +
				this.attribute.name +
				'="' +
				this.attribute.value +
				'>' +
				this.children.render() +
				'</' +
				this.tagName +
				'>'
			);
		} else {
			let tmp = this.renderChildren();
			return tmp;
		}
	}
}
