import data from './data.js';
import Component from './components/Component.js';
import Img from './components/Img.js';

const title = new Component('h1', null, ['La', ' ', 'carte']);
console.log(title.render());
document.querySelector('.pageTitle').innerHTML = title.render();
const img = new Img(
	'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
);
console.log(img.render());
document.querySelector('.pageContent').innerHTML = img.render();
/*const c = new Component('article', { name: 'class', value: 'pizzaThumbnail' }, [
	new Img(
		'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	),
]);
console.log(c.render());
document.querySelector('.pageContent').innerHTML = c.render();*/
